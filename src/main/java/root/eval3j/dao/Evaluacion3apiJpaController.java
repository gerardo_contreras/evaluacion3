/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.eval3j.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import root.eval3j.dao.exceptions.NonexistentEntityException;
import root.eval3j.dao.exceptions.PreexistingEntityException;
import root.eval3j.entity.Evaluacion3api;

/**
 *
 * @author Gera
 */
public class Evaluacion3apiJpaController implements Serializable {

    public Evaluacion3apiJpaController() {     
    }
    EntityManagerFactory emf= Persistence.createEntityManagerFactory("eval3_PU");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Evaluacion3api evaluacion3api) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(evaluacion3api);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findEvaluacion3api(evaluacion3api.getCabana()) != null) {
                throw new PreexistingEntityException("Evaluacion3api " + evaluacion3api + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Evaluacion3api evaluacion3api) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            evaluacion3api = em.merge(evaluacion3api);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = evaluacion3api.getCabana();
                if (findEvaluacion3api(id) == null) {
                    throw new NonexistentEntityException("The evaluacion3api with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Evaluacion3api evaluacion3api;
            try {
                evaluacion3api = em.getReference(Evaluacion3api.class, id);
                evaluacion3api.getCabana();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The evaluacion3api with id " + id + " no longer exists.", enfe);
            }
            em.remove(evaluacion3api);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Evaluacion3api> findEvaluacion3apiEntities() {
        return findEvaluacion3apiEntities(true, -1, -1);
    }

    public List<Evaluacion3api> findEvaluacion3apiEntities(int maxResults, int firstResult) {
        return findEvaluacion3apiEntities(false, maxResults, firstResult);
    }

    private List<Evaluacion3api> findEvaluacion3apiEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Evaluacion3api.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Evaluacion3api findEvaluacion3api(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Evaluacion3api.class, id);
        } finally {
            em.close();
        }
    }

    public int getEvaluacion3apiCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Evaluacion3api> rt = cq.from(Evaluacion3api.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
