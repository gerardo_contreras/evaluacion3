/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.eval3j.appconfig;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;


/**
 * Configures JAX-RS for the application.
 * @author gcontreras
 */
@ApplicationPath("LaAPI")
public class AppConfig extends Application {
    
}
