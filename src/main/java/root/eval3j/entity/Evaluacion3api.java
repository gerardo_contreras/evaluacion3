/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.eval3j.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Gera
 */
@Entity
@Table(name = "evaluacion3api")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Evaluacion3api.findAll", query = "SELECT e FROM Evaluacion3api e"),
    @NamedQuery(name = "Evaluacion3api.findByCabana", query = "SELECT e FROM Evaluacion3api e WHERE e.cabana = :cabana"),
    @NamedQuery(name = "Evaluacion3api.findByOcupantes", query = "SELECT e FROM Evaluacion3api e WHERE e.ocupantes = :ocupantes"),
    @NamedQuery(name = "Evaluacion3api.findByBanos", query = "SELECT e FROM Evaluacion3api e WHERE e.banos = :banos"),
    @NamedQuery(name = "Evaluacion3api.findByCamas", query = "SELECT e FROM Evaluacion3api e WHERE e.camas = :camas")})
public class Evaluacion3api implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "cabana")
    private String cabana;
    @Size(max = 10)
    @Column(name = "ocupantes")
    private String ocupantes;
    @Size(max = 10)
    @Column(name = "banos")
    private String banos;
    @Size(max = 10)
    @Column(name = "camas")
    private String camas;

    public Evaluacion3api() {
    }

    public Evaluacion3api(String cabana) {
        this.cabana = cabana;
    }

    public String getCabana() {
        return cabana;
    }

    public void setCabana(String cabana) {
        this.cabana = cabana;
    }

    public String getOcupantes() {
        return ocupantes;
    }

    public void setOcupantes(String ocupantes) {
        this.ocupantes = ocupantes;
    }

    public String getBanos() {
        return banos;
    }

    public void setBanos(String banos) {
        this.banos = banos;
    }

    public String getCamas() {
        return camas;
    }

    public void setCamas(String camas) {
        this.camas = camas;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cabana != null ? cabana.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Evaluacion3api)) {
            return false;
        }
        Evaluacion3api other = (Evaluacion3api) object;
        if ((this.cabana == null && other.cabana != null) || (this.cabana != null && !this.cabana.equals(other.cabana))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "root.eval3j.entity.Evaluacion3api[ cabana=" + cabana + " ]";
    }
    
}
