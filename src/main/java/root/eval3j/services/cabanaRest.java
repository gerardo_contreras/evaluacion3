/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.eval3j.services;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.eval3j.dao.Evaluacion3apiJpaController;
import root.eval3j.dao.exceptions.NonexistentEntityException;
import root.eval3j.entity.Evaluacion3api;

/**
 *
 * @author Gera
 */
@Path("cabanas")
public class cabanaRest{
   
 
@GET    
@Produces(MediaType.APPLICATION_JSON)    

public Response listacabanas(){
    
        Evaluacion3apiJpaController dao=new Evaluacion3apiJpaController();
    
     List<Evaluacion3api> evaluacion3api= dao.findEvaluacion3apiEntities();
     
     return Response.ok(200).entity(evaluacion3api).build();
           }

@POST
@Produces(MediaType.APPLICATION_JSON)
public Response Crear(Evaluacion3api evaluacion3api) {

    Evaluacion3apiJpaController dao=new Evaluacion3apiJpaController();
    try {
        dao.create(evaluacion3api);
    } catch (Exception ex) {
        Logger.getLogger(cabanaRest.class.getName()).log(Level.SEVERE, null, ex);
    }
     return Response.ok(200).entity(evaluacion3api).build();
}

@PUT
@Produces(MediaType.APPLICATION_JSON)
public Response actualizar(Evaluacion3api evaluacion3api){
    
    Evaluacion3apiJpaController dao = new Evaluacion3apiJpaController();
    try {
        dao.edit(evaluacion3api);
    } catch (Exception ex) {
        Logger.getLogger(cabanaRest.class.getName()).log(Level.SEVERE, null, ex);
    }
      
      return Response.ok(200).entity(evaluacion3api).build();
}

@DELETE
@Path("/{ideliminar}")
@Produces(MediaType.APPLICATION_JSON)
public Response eliminar(@PathParam("ideliminar") String ideliminar){

    Evaluacion3apiJpaController dao= new Evaluacion3apiJpaController();
    try {
        dao.destroy(ideliminar);
    } catch (NonexistentEntityException ex) {
        Logger.getLogger(cabanaRest.class.getName()).log(Level.SEVERE, null, ex);
    }
    return Response.ok("eliminada").build();
}

@GET
@Path("/{idconsultar}")
@Produces(MediaType.APPLICATION_JSON)
public Response consultarPorId(@PathParam("idconsultar") String idconsultar){

    Evaluacion3apiJpaController dao=new Evaluacion3apiJpaController();
    Evaluacion3api evaluacion3api= dao.findEvaluacion3api(idconsultar);
    
    return Response.ok(200).entity(evaluacion3api).build();

}

}
